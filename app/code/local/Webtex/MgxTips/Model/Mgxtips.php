<?php
/**
 * Webtex
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.webtexsoftware.com/LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@webtexsoftware.com and we will send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to http://www.webtexsoftware.com for more information, 
 * or contact us through this email: info@webtexsoftware.com.
 *
 * @category   Webtex
 * @package    Webtex_Tips
 * @copyright  Copyright (c) 2011 Webtex Solutions, LLC (http://www.webtexsoftware.com/)
 * @license    http://www.webtexsoftware.com/LICENSE.txt End-User License Agreement
 */
 
class Webtex_MgxTips_Model_MgxTips extends Mage_Core_Model_Abstract
{
    private $aPositions = array(
        'l' => 'left',
        'r' => 'right',
        't' => 'top',
        'b' => 'bottom',
    );
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('mgxtips/mgxtips');
    }
   
    public function getLabels()
    {
        $this->_getResource();
        return $this;                
            
    }
   
   
    
    public function getImageSrc()
    {
        $sSrc = Mage::getStoreConfig('webtex_mgxtips/mgxtips/upload');
        $sBaseUrl = Mage::getBaseUrl('media') . 'webtex/mgxtips/';
        
        if("" != $sSrc)
        {
            return $sBaseUrl . $sSrc;
        }
        else
        {
            return $sBaseUrl . 'default/default.png';
        }
    }
    
    
    public function getPositionTips()
    {
        $sPosition = Mage::getStoreConfig('webtex_mgxtips/mgxtips/position');
        return "'" . $this->_getConvertedPosition($sPosition) . "'";
    }
    
    public function getFageEffect()
    {
        $bFadeFlag = Mage::getStoreConfig('webtex_mgxtips/mgxtips/fade');
        
        if($bFadeFlag) 
        {
            return "'fade'";
        }
        else
        {
            return "'none'";
        }
    }
    
    public function getMetod()
    {
        $bDisplay = Mage::getStoreConfig('webtex_mgxtips/mgxtips/metod');
        
        if($bDisplay == 0) 
        {
            return ', fixed: false';
        }
        elseif($bDisplay == 1)
        {
            return '';
        }
        elseif($bDisplay == 2)
        {
            return ', persistent: true';
        }
    }
    
    private function _getConvertedPosition($sPosition) {
    	return ($sPosition !== '') ? $this->aPositions[$sPosition] : 'right'; 
    }
}