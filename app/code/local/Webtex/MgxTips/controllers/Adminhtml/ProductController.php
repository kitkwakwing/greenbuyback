<?php
/**
 * Webtex
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.webtexsoftware.com/LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@webtexsoftware.com and we will send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to http://www.webtexsoftware.com for more information, 
 * or contact us through this email: info@webtexsoftware.com.
 *
 * @category   Webtex
 * @package    Webtex_Tips
 * @copyright  Copyright (c) 2011 Webtex Solutions, LLC (http://www.webtexsoftware.com/)
 * @license    http://www.webtexsoftware.com/LICENSE.txt End-User License Agreement
 */

require_once 'Mage/Adminhtml/controllers/Catalog/ProductController.php';

class Webtex_MgxTips_Adminhtml_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    public function saveAction()
    {
        $storeId        = $this->getRequest()->getParam('store', 0);
        $productData = $this->getRequest()->getPost('product');
        $options = $productData['options'];
        $mgxoptions = $this->getRequest()->getPost('mgxoptions');
        $mgxtips = $this->getRequest()->getPost('mgxtips');
		$mgxtipsforopt = $this->getRequest()->getPost('mgxtipsforopt');
        
        foreach($options as $key => $value)
        {
            $model = Mage::getModel('mgxtips/options');
            
            $data['value'] = $mgxtips[$key];
            $data['option_id'] = $key;
            $data['store_id'] = $storeId;
            
            $model->setData($data);
            
            $collection = Mage::getModel('mgxtips/options')->getCollection()
                    ->addFieldToFilter('option_id', $data['option_id'])
                    ->addFieldToFilter('store_id', $data['store_id']);
            
            if($collection->getSize())
            {
                foreach($collection as $val)
                {
                    $model->setId($val->getId());
                }
            }
            
            try
            {
                $model->save();
            } 
            catch (Exception $e){}
        
        }
        
        
        $model = Mage::getModel('mgxtips/options_type');
        
        foreach($mgxoptions as $key => $value)
        {
            foreach ($value as $k => $val)
            {
                $data1['option_type_id'] = $key;
                $data1['option_type_select_id'] = $k;
                $data1['store_id'] = $storeId;
                $data1['value'] = $val;
            
                $model->setData($data1);
            
                $collection = Mage::getModel('mgxtips/options_type')->getCollection()
                    ->addFieldToFilter('option_type_id', $data1['option_type_id'])
                    ->addFieldToFilter('option_type_select_id', $data1['option_type_select_id'])
                    ->addFieldToFilter('store_id', $data['store_id']);
            
                if($collection->getSize())
                {
                    foreach($collection as $val1)
                    {
                        $model->setId($val1->getId());
                    }
                }
            
                try
                {
                    $model->save();
                } 
                catch (Exception $e){}
            
            }
        }
        
        parent::saveAction();
    }
        
}
