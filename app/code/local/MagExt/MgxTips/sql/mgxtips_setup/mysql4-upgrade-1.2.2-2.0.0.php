<?php
/**
 * Webtex
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.webtexsoftware.com/LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@webtexsoftware.com and we will send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to http://www.webtexsoftware.com for more information, 
 * or contact us through this email: info@webtexsoftware.com.
 *
 * @category   design_default
 * @package    MagExt_Tips
 * @copyright  Copyright (c) 2011 Webtex Solutions, LLC (http://www.webtexsoftware.com/)
 * @license    http://www.webtexsoftware.com/LICENSE.txt End-User License Agreement
 */
 
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('mgxtips_options_type')};
CREATE TABLE {$this->getTable('mgxtips_options_type')} (
  `mgxtips_option_type_id` int(10) unsigned NOT NULL auto_increment,
  `option_type_id` int(10) unsigned NOT NULL default '0',
  `option_type_select_id` int(10) unsigned NOT NULL default '0',
  `store_id` int(10) unsigned NOT NULL default '0',
  `value` TEXT NOT NULL,
  PRIMARY KEY  (`mgxtips_option_type_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


 DROP TABLE IF EXISTS {$this->getTable('mgxtips_templates')};
CREATE TABLE {$this->getTable('mgxtips_templates')} (
  `mgxtips_templates_id` int(10) unsigned NOT NULL auto_increment,
  `option_id` TEXT NOT NULL,
  `value` TEXT NOT NULL,
  `store_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`mgxtips_templates_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


    ");

$installer->endSetup(); 