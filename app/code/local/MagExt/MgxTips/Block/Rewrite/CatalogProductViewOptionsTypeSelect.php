<?php
/**
 * Webtex
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.webtexsoftware.com/LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@webtexsoftware.com and we will send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to http://www.webtexsoftware.com for more information, 
 * or contact us through this email: info@webtexsoftware.com.
 *
 * @category   MagExt
 * @package    MagExt_Tips
 * @copyright  Copyright (c) 2011 Webtex Solutions, LLC (http://www.webtexsoftware.com/)
 * @license    http://www.webtexsoftware.com/LICENSE.txt End-User License Agreement
 */
class MagExt_MgxTips_Block_Rewrite_CatalogProductViewOptionsTypeSelect
    extends Mage_Catalog_Block_Product_View_Options_Type_Select
{

    public function getValuesHtml()
    {
        
        $sSwitcher = Mage::getStoreConfig('magext_mgxtips/mgxtips/switcher');
        
        $_option = $this->getOption();

        if ($_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN
            || $_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_MULTIPLE) {
            $require = ($_option->getIsRequire()) ? ' required-entry' : '';
            $extraParams = '';
            $select = $this->getLayout()->createBlock('core/html_select')
                ->setData(array(
                    'id' => 'select_'.$_option->getId(),
                    'class' => $require.' product-custom-option'
                ));
            if ($_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN) {
                $select->setName('options['.$_option->getid().']')
                    ->addOption('', $this->__('-- Please Select --'));
            } else {
                $select->setName('options['.$_option->getid().'][]');
                $select->setClass('multiselect'.$require.' product-custom-option');
            }
            foreach ($_option->getValues() as $_value) {
                $priceStr = $this->_formatPrice(array(
                    'is_percent' => ($_value->getPriceType() == 'percent') ? true : false,
                    'pricing_value' => $_value->getPrice(true)
                ), false);
                $select->addOption(
                    $_value->getOptionTypeId(),
                    $_value->getTitle() . ' ' . $priceStr . ''
                );
            }
            if ($_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_MULTIPLE) {
                $extraParams = ' multiple="multiple"';
            }
              
            
            
            
            
            $select->setExtraParams('onchange="opConfig.reloadPrice()"'.$extraParams);
            //$select->setExtraParams('onchange="opConfig.reloadPrice(); mgxchanger('.$_option->getId().');"'.$extraParams);

            $html = $select->getHtml();

            return $html; //. '<div class="mgxicon_'.$_option->getId().'"><img class="tipsyicon" src="' . Mage::getModel('mgxtips/mgxtips')->getImageSrc(). '" original-title="rr" /></div>';
        }

        if ($_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO
            || $_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_CHECKBOX
            ) {
            $selectHtml = '<ul id="options-'.$_option->getId().'-list" class="options-list">';
            $require = ($_option->getIsRequire()) ? ' validate-one-required-by-name' : '';
            $arraySign = '';
            switch ($_option->getType()) {
                case Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO:
                    $type = 'radio';
                    $class = 'radio';
                    if (!$_option->getIsRequire()) {
                        $selectHtml .= '<li><input type="radio" id="options_'.$_option->getId().'" class="'.$class.' product-custom-option" name="options['.$_option->getId().']" onclick="opConfig.reloadPrice()" value="" checked="checked" /><span class="label"><label for="options_'.$_option->getId().'">' . $this->__('None') . '</label></span></li>';
                    }
                    break;
                case Mage_Catalog_Model_Product_Option::OPTION_TYPE_CHECKBOX:
                    $type = 'checkbox';
                    $class = 'checkbox';
                    $arraySign = '[]';
                    break;
            }
            $count = 1;
            foreach ($_option->getValues() as $_value) {
                $count++;
                $priceStr = $this->_formatPrice(array(
                    'is_percent' => ($_value->getPriceType() == 'percent') ? true : false,
                    'pricing_value' => $_value->getPrice(true)
                ));
                $selectHtml .= '<li>' .
                               '<input type="'.$type.'" class="'.$class.' '.$require.' product-custom-option" onclick="opConfig.reloadPrice()" name="options['.$_option->getId().']'.$arraySign.'" id="options_'.$_option->getId().'_'.$count.'" value="'.$_value->getOptionTypeId().'" />'.
                               '<span class="label"><label for="options_'.$_option->getId().'_'.$count.'"';
                               if(!Mage::getStoreConfig('magext_mgxtips/mgxtips/metod')) $selectHtml .= ' class="tipsyiconw_'.$_option->getId().'_'.$_value->getOptionTypeId().'"';
                $selectHtml .= '>'.$_value->getTitle().' '.$priceStr.'</label>';
                               
                               
                        
                        $collection = Mage::getModel('mgxtips/options_type')->getCollection()
                        ->addFieldToFilter('option_type_id', $_option->getId())
                        ->addFieldToFilter('option_type_select_id', $_value->getOptionTypeId())
                        ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
                        
                        
                        $mgxtip = $collection->getFirstItem()->getValue();
                        
                        if(Mage::app()->getStore()->getId() != 0 && $mgxtip == '')
                        {
                            $collection = Mage::getModel('mgxtips/options_type')->getCollection()
                            ->addFieldToFilter('option_type_id', $_option->getId())
                            ->addFieldToFilter('option_type_select_id', $_value->getOptionTypeId())
                            ->addFieldToFilter('store_id', 0);
                            
                            $mgxtip = $collection->getFirstItem()->getValue();
                        }
                        
                        if(isset($mgxtip) && trim($mgxtip) != "" && trim($mgxtip) != "<br>" && ($sSwitcher == 'all' || $sSwitcher == 'product-page')) :       
                            $selectHtml .= '<div class="tipsyiconw_'.$_option->getId().'_'.$_value->getOptionTypeId().' tipsyicon"><img src="' . Mage::getModel('mgxtips/mgxtips')->getImageSrc(). '" /></div>';
                            $selectHtml .= "<script>mgxQuery('.tipsyiconw_".$_option->getId()."_".$_value->getOptionTypeId()."').simpletip({content: '".addslashes($this->__($mgxtip))."',  position: ".Mage::getModel('mgxtips/mgxtips')->getPositionTips().", showEffect: ".Mage::getModel('mgxtips/mgxtips')->getFageEffect().", hideEffect: 'none'".Mage::getModel('mgxtips/mgxtips')->getMetod()."});</script>";
                         endif;
                               
                               
                               
                               $selectHtml .= '</span>';
                if ($_option->getIsRequire()) {
                    $selectHtml .= '<script type="text/javascript">' .
                                    '$(\'options_'.$_option->getId().'_'.$count.'\').advaiceContainer = \'options-'.$_option->getId().'-container\';' .
                                    '$(\'options_'.$_option->getId().'_'.$count.'\').callbackFunction = \'validateOptionsCallback\';' .
                                   '</script>';
                }
                $selectHtml .= '</li>';
            }
            $selectHtml .= '</ul>';
            return $selectHtml;
        }
    }
    
    
    public function getMgxOptions()
    {
        $_option = $this->getOption();
        
        
        $collection = Mage::getModel('mgxtips/options_type')->getCollection()
                            ->addFieldToFilter('option_type_id', $_option->getId())
                            ->addFieldToFilter('store_id', 0);
            
        $otarray = array();
            
        foreach($collection as $item)
        {
            $otarray[$item->getOptionTypeSelectId()] = $item->getValue();
        }
        
        return $otarray;
    }

}
