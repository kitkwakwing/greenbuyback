<?php
/**
 * Webtex
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.webtexsoftware.com/LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@webtexsoftware.com and we will send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to http://www.webtexsoftware.com for more information, 
 * or contact us through this email: info@webtexsoftware.com.
 *
 * @category   MagExt
 * @package    MagExt_Tips
 * @copyright  Copyright (c) 2011 Webtex Solutions, LLC (http://www.webtexsoftware.com/)
 * @license    http://www.webtexsoftware.com/LICENSE.txt End-User License Agreement
 */

class MagExt_MgxTips_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {

        $this->loadLayout();
        
        $this->_setActiveMenu('mgxtips');

        $this->_addContent($this->getLayout()->createBlock('mgxtips/adminhtml_templ_toolbar'));
        $this->_addContent($this->getLayout()->createBlock('mgxtips/adminhtml_templ_grid'));

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }
    
    public function addAction()
    {

        $this->loadLayout();
        
        $this->_setActiveMenu('mgxtips');

        $this->_addContent($this->getLayout()->createBlock('mgxtips/adminhtml_add_toolbar'));
        $this->_addContent($this->getLayout()->createBlock('mgxtips/adminhtml_add_body'));

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }
    
    public function editAction()
    {

        $this->loadLayout();
        
        $this->_setActiveMenu('mgxtips');

        $this->_addContent($this->getLayout()->createBlock('mgxtips/adminhtml_edit_toolbar'));
        $this->_addContent($this->getLayout()->createBlock('mgxtips/adminhtml_edit_body'));

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }
    
    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        $params = $this->getRequest()->getParams();
        $value = $data['value'];
        $error = false;
        
        foreach($value as $key => $val)
        {
            $data['store_id'] = $key;
            $data['value'] = $val;
            
            if (isset($data['option_id']) && isset($data['value']) && $data['option_id'])
            {               
                $model = Mage::getModel('mgxtips/templates');
                $model->setData($data);
            
                try 
                {
                    $model->save();
                }
                catch(Exception $e)
                {
                    $error = true;
                }
            }
        }
        
        if(isset($error) && $error)
        {
            Mage::getSingleton('customer/session')->addError($this->__('Ooops! Something wrong. Template was not saved.'));
        }
        else
        {
            Mage::getSingleton('customer/session')->addSuccess($this->__('Template was successfully saved.'));
        }
        
        $this->_redirect('*/*/index');
    }
    
    
    public function saveedAction()
    {
        $data = $this->getRequest()->getPost();
        $params = $this->getRequest()->getParams();
        $value = $data['value'];
        $error = false;
        
        foreach($value as $key => $val)
        {
            $data['value'] = $val;
            
            if (isset($data['value']))
            {               
                $model = Mage::getModel('mgxtips/templates');
                $model->setData($data);
                $model->setId($key);
            
                try 
                {
                    $model->save();
                }
                catch(Exception $e)
                {
                    $error = true;
                }
            }
        }
        
        if(isset($error) && $error)
        {
            Mage::getSingleton('customer/session')->addError($this->__('Ooops! Something wrong. Template was not saved.'));
        }
        else
        {
            Mage::getSingleton('customer/session')->addSuccess($this->__('Template was successfully saved.'));
        }
        
        $this->_redirect('*/*/index');
    }
    
    public function deleteAction()
    {
        $data = $this->getRequest()->getParams();
        
        if (isset($data['template_id']))
        {
            $model = Mage::getModel('mgxtips/templates');
            $model->setId($data['template_id']);
            
            try 
            {
                $model->delete();
                Mage::getSingleton('customer/session')->addSuccess($this->__('Template was successfully deleted.'));
            }
            catch(Exception $e)
            {
                Mage::getSingleton('customer/session')->addError($this->__('Ooops! Something wrong. Template was not deleted.'));
            }
        }
        
        $this->_redirect('*/*/index');
    }
}