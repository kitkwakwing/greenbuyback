<?php
/**
 * Loyalty Program
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitloyalty
 * @version      2.3.17
 * @license:     Kp3auyVhQ0x7OAufhTieSqVe2sWXaoRky2ijjO5VCk
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 *
 * @copyright  Copyright (c) 2011 AITOC, Inc.
 * @package    Aitoc_Aitloyalty
 * @author lyskovets
 */
abstract class Aitoc_Aitloyalty_Model_Observer_Abstract extends Varien_Object
{
    abstract public function process(Varien_Event_Observer $event);
}