<?php
/**
 * Loyalty Program
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitloyalty
 * @version      2.3.17
 * @license:     Kp3auyVhQ0x7OAufhTieSqVe2sWXaoRky2ijjO5VCk
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 *
 * @copyright  Copyright (c) 2011 AITOC, Inc.
 * @package    Aitoc_Aitloyalty
 * @author lyskovets
 */
class Aitoc_Aitloyalty_Helper_Discount extends Mage_Core_Helper_Abstract
{
    public function getTitlePart($amount)
    {
        $part = ($amount > 0)?'Surcharge':'Discount';
        return $part;
    }
}