<?php
/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php
class Infomodus_Upslabelinv_LabelController extends Mage_Core_Controller_Front_Action
{

    public function preDispatch()
    {
        if(Mage::getStoreConfig('upslabelinv/labeloptions/guest')==0){
            if (!Mage::getSingleton('customer/session')->authenticate($this)) {
                parent::preDispatch();
                $this->setFlag('', 'no-dispatch', true);
            }
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $order_id = $this->getRequest()->getParam('order_id');
        $order = Mage::getSingleton('sales/order')->load($order_id);
        $customerId = $order->getCustomerId();
        if ($customerId == Mage::getSingleton('customer/session')->getId() || Mage::getStoreConfig('upslabelinv/labeloptions/guest')==1) {
            $labels1 = Mage::getModel('upslabelinv/upslabelinv')->getCollection()->addFieldToFilter('type', 'from')->addFieldToFilter('order_id', $order_id);
            if ($labels1->getSize() > 0) {
                $labels = $labels1->getData();
                $labels = $labels[0];
            }
            echo str_replace('./label', Mage::getBaseUrl('media') . 'upslabelinv/label/label', file_get_contents(Mage::getBaseUrl('media') . 'upslabelinv/label/' . $labels['trackingnumber'].'.html'));
        }
    }
}