<?php

/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */

class Infomodus_Upslabelinv_Adminhtml_PdflabelsController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $order_ids = $this->getRequest()->getParam('order_ids');
        $img_path = Mage::getBaseDir('media') .'/upslabelinv/label/';
        $url_image_path = Mage::getBaseUrl('media') .'upslabelinv/label/';
        $pdf = new Zend_Pdf();
        $i=0;
        //$pdf->pages = array_reverse($pdf->pages);
        if(!is_array($order_ids)){
            $order_ids = explode(',', $order_ids);
        }
        foreach ($order_ids as $order_id) {
            $collection = Mage::getModel('upslabelinv/upslabelinv')->load($order_id, 'order_id');
            if ($collection->getOrderId() == $order_id) {
                //echo $collection->getLabelname();
                if (file_exists($img_path. $collection->getLabelname())) {
                    $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
                    $pdf->pages[] = $page;
                    $f_cont = file_get_contents($img_path. $collection->getLabelname());
                    $img = imagecreatefromstring($f_cont);
                    //if (!$img) {echo 'Error';}
                    $rnd = rand(10000, 999999);
                    imagejpeg($img, $img_path.'lbl'.$rnd.'.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath($img_path.'lbl'.$rnd.'.jpeg');
                    //$image = Zend_Pdf_ImageFactory::factory($img_path . $collection->getLabelname());
                    $page->drawImage($image, 10, 10, 1400/2.6, 800/2.6);
                    unlink($img_path.'lbl'.$rnd.'.jpeg');
                    $i++;
                } 
            }
        }
        //$pdf->save();
        if($i>0){
         $pdfData = $pdf->render();

          header("Content-Disposition: inline; filename=result.pdf");
          header("Content-type: application/x-pdf");
          echo $pdfData;
        }
    }
    public function onepdfAction()
        {
            $order_id = $this->getRequest()->getParam('order_id');
            $type = $this->getRequest()->getParam('type');
            $img_path = Mage::getBaseDir('media') . '/upslabelinv/label/';
            $url_image_path = Mage::getBaseUrl('media') . 'upslabelinv/label/';
            $pdf = new Zend_Pdf();
            $i = 0;
            $collections = Mage::getModel('upslabelinv/upslabelinv');
            $colls = $collections->getCollection()->addFieldToFilter('order_id', $order_id)->addFieldToFilter('type', $type);
            foreach ($colls AS $k => $v) {
                $coll = $k;
                break;
            }
            $collection = Mage::getModel('upslabelinv/upslabelinv')->load($coll);
            if ($collection->getOrderId() == $order_id) {
                if (file_exists($img_path . $collection->getLabelname())) {
                    $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);/*Zend_Pdf_Page::SIZE_A4*/
                    $pdf->pages[] = $page;
                    $f_cont = file_get_contents($img_path . $collection->getLabelname());
                    $img = imagecreatefromstring($f_cont);
                    //if (!$img) {echo 'Error';}
                    $rnd = rand(10000, 999999);
                    imagejpeg($img, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                    //$image = Zend_Pdf_ImageFactory::factory($img_path . $collection->getLabelname());
                    $page->drawImage($image, 10, 10, 1400 / 2.6, 800 / 2.6);
                    unlink($img_path . 'lbl' . $rnd . '.jpeg');
                    $i++;
                }
            }
            if ($i > 0) {
                $pdfData = $pdf->render();

                header("Content-Disposition: inline; filename=result.pdf");
                header("Content-type: application/x-pdf");
                echo $pdfData;
            }
        }
}
