<?php
/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php

class Infomodus_Upslabelinv_Block_Sales_Order_Shipment_Create extends Mage_Adminhtml_Block_Sales_Order_Shipment_Create
{
    public function __construct()
    {
        /*Mage::log(__METHOD__);*/
        $this->_objectId = 'order_id';
        $this->_controller = 'sales_order_shipment';
        $this->_mode = 'create';

        parent::__construct();

        //$this->_updateButton('save', 'label', Mage::helper('sales')->__('Submit Shipment'));
        $this->_removeButton('save');
        $this->_removeButton('delete');
        $order_idd = $this->getRequest()->getParam('order_id');
        if ($this->getRequest()->getParam('order_id')) {
            $order = Mage::getModel('sales/order')->load($order_idd);
            $ship_method = $order->getShippingMethod();
            $shipByUps = preg_replace("/^ups_.{2,4}$/", 'ups', $ship_method);
            $onlyups = Mage::getStoreConfig('upslabelinv/profile/onlyups');
            $collection = Mage::getModel('upslabelinv/upslabelinv')->getCollection()->addFieldToFilter('order_id', $order_idd);
            $shipfromCountryCode = Mage::getStoreConfig('upslabelinv/shipfrom/countrycode');
            $shiping_adress = $order->getShippingAddress();
            $shiptoCountryCode = $shiping_adress['country_id'];


            $shipMethodArray = explode('_', $order->getShippingMethod());
            if ($shipByUps == 'ups' || $onlyups == 0 || $shipMethodArray[0] == 'upstablerates') {
                $shipWay = 0;
                if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
                    $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
                    $shipWay = $upstablerates['way'];
                }
                $namePromPageTo = 'intermediate';
                $namePromPageFrom = 'intermediate';
                $namePromPageTo2 = 'intermediate';
                foreach ($collection AS $k => $v) {
                    switch ($v->getType()) {
                        case 'to':
                            $namePromPageTo = 'showlabel';
                            break;
                        case 'from':
                            $namePromPageFrom = 'showlabel';
                            break;
                        case 'to2':
                            $namePromPageTo2 = 'showlabel';
                            break;
                    }

                }
                if ((int)$shipWay == 3) {
                    $this->_addButton('order_label_to', array(
                        'label' => Mage::helper('sales')->__('UPS Label to'),
                        'onclick' => 'setLocation(\'' . $this->getUrl('upslabelinv/adminhtml_upslabelinv/' . $namePromPageTo . '/order_id/' . $this->getShipment()->getOrderId() . '/type/to') . '\')',
                        'class' => 'go'
                    ));
                }
                $this->_addButton('order_label_from', array(
                    'label' => Mage::helper('sales')->__('UPS Label from'),
                    'onclick' => 'setLocation(\'' . $this->getUrl('upslabelinv/adminhtml_upslabelinv/' . $namePromPageFrom . '/order_id/' . $this->getShipment()->getOrderId() . '/type/from') . '\')',
                    'class' => 'go'
                ));
                $this->_addButton('order_label_to2', array(
                    'label' => Mage::helper('sales')->__('UPS Label to 2'),
                    'onclick' => 'setLocation(\'' . $this->getUrl('upslabelinv/adminhtml_upslabelinv/' . $namePromPageTo2 . '/order_id/' . $this->getShipment()->getOrderId() . '/type/to2') . '\')',
                    'class' => 'go'
                ));
            }
        }
    }
}
