<?php
/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php

class Infomodus_Upslabelinv_Block_Sales_Order_Shipment_View extends Mage_Adminhtml_Block_Sales_Order_Shipment_View
{

    public function __construct()
    {
        parent::__construct();
        $ship_id = $this->getShipment()->getId();
        if ($ship_id) {
            $order = Mage::getModel('sales/order')->load($this->getShipment()->getOrderId());
            $ship_method = $order->getShippingMethod();
            $shipByUps = preg_replace("/^ups_.{2,4}$/", 'ups', $ship_method);
            $onlyups = Mage::getStoreConfig('upslabelinv/profile/onlyups');
            $collection = Mage::getModel('upslabelinv/upslabelinv')->getCollection()->addFieldToFilter('order_id', $order);

            $shipMethodArray = explode('_', $order->getShippingMethod());
            $shipWay = 0;
            if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
                $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
                $shipWay = $upstablerates['way'];
            }

            if ($shipByUps == 'ups' || $onlyups == 0 || $shipMethodArray[0] == 'upstablerates') {
                $namePromPageTo = 'intermediate';
                $namePromPageFrom = 'intermediate';
                $namePromPageTo2 = 'intermediate';
                foreach ($collection AS $k => $v) {
                    switch ($v->getType()) {
                        case 'to':
                            $namePromPageTo = 'showlabel';
                            break;
                        case 'from':
                            $namePromPageFrom = 'showlabel';
                            break;
                        case 'to2':
                            $namePromPageTo2 = 'showlabel';
                            break;
                    }

                }
                if ((int)$shipWay == 3) {
                    $this->_addButton('order_label_to', array(
                        'label' => Mage::helper('sales')->__('UPS Label to'),
                        'onclick' => 'setLocation(\'' . $this->getUrl('upslabelinv/adminhtml_upslabelinv/' . $namePromPageTo . '/order_id/' . $this->getShipment()->getOrderId() . '/type/to') . '\')',
                        'class' => 'go'
                    ));
                }
                $this->_addButton('order_label_from', array(
                    'label' => Mage::helper('sales')->__('UPS Label from'),
                    'onclick' => 'setLocation(\'' . $this->getUrl('upslabelinv/adminhtml_upslabelinv/' . $namePromPageFrom . '/order_id/' . $this->getShipment()->getOrderId() . '/type/from') . '\')',
                    'class' => 'go'
                ));
                $this->_addButton('order_label_to2', array(
                    'label' => Mage::helper('sales')->__('UPS Label to 2'),
                    'onclick' => 'setLocation(\'' . $this->getUrl('upslabelinv/adminhtml_upslabelinv/' . $namePromPageTo2 . '/order_id/' . $this->getShipment()->getOrderId() . '/type/to2') . '\')',
                    'class' => 'go'
                ));
            }
        }
    }
}
