<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Owner
 * Date: 16.02.12
 * Time: 16:07
 * To change this template use File | Settings | File Templates.
 */
class Infomodus_Upslabelinv_Helper_Help extends Mage_Core_Helper_Abstract
{
    static public function escapeXML($string){
        $string = preg_replace('/&/is','&amp;',$string);
        $string = preg_replace('/</is','&lt;',$string);
        $string = preg_replace('/>/is','&gt;',$string);
        $string = preg_replace('/\'/is','&apos;',$string);
        $string = preg_replace('/"/is','&quot;',$string);
        return trim($string);
    }
}
