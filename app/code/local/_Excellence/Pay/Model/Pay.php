<?php
class Excellence_Pay_Model_Pay extends Mage_Payment_Model_Method_Abstract
{
	protected $_code = 'pay';
	protected $_formBlockType = 'pay/form_pay';
	protected $_infoBlockType = 'pay/info_pay';
    // TSD added the following line 
    // protected $errorMsg = $this->_getHelper()->__('PAYPAL EMAIL is a required field');

	public function assignData($data)
	{
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info->setCheckNo($data->getCheckNo())
		->setCheckDate($data->getCheckDate());
		return $this;
	}


	public function validate()
	{
		parent::validate();

		$info = $this->getInfoInstance();

		$no = $info->getCheckNo();
		/*$date = $info->getCheckDate();*/
		if(empty($no)){
			$errorCode = 'invalid_data';
			$errorMsg = $this->_getHelper()->__('PAYPAL EMAIL is a required field');
		}

		if($errorMsg){
			Mage::throwException($errorMsg);
		}


		return $this;
	}
}
?>
