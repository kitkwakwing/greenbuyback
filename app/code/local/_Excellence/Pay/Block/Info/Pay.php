<?php
class Excellence_Pay_Block_Info_Pay extends Mage_Payment_Block_Info
{

	require_once("app/Mage.php");
    Mage::app('default');

    $myOrder=Mage::getModel('sales/order'); 
    $orders=Mage::getModel('sales/mysql4_order_collection');
    $allIds=$orders->getAllIds();
    foreach($allIds as $thisId) {
        $myOrder->load($thisId);
        echo "name: ". $myOrder->getShippingAddress()->getFirstname() . " " . $myOrder->getShippingAddress()->getLastname();
        echo "email: " . $myOrder->getPayment()->getOrder()->getEmail();
    }



	protected function _prepareSpecificInformation($transport = null)
	{
		if (null !== $this->_paymentSpecificInformation) {
			return $this->_paymentSpecificInformation;
		}
		$info = $this->getInfo();
		$transport = new Varien_Object();
		$transport = parent::_prepareSpecificInformation($transport);
		$transport->addData(array(
			Mage::helper('payment')->__('PAYPAL EMAIL') => $info->getCheckNo()/*,
			Mage::helper('payment')->__('Check Date') => $info->getCheckDate()*/
		));
		return $transport;
	}
}