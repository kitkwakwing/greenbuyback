<style type="text/css">
	.green{color:green;}
	.red{color:red;}
	img{width:10px;height: 10px;}
	img:hover{width:100px;height:100px;position: absolute;}
</style>
<?php if ( function_exists( 'mail' ) )
{
    echo 'mail() is available';
}
else
{
    echo 'mail() has been disabled';
}  ?>
<?php
extension_check(array( 
	'curl',
	'dom', 
	'gd', 
	'hash',
	'iconv',
	'mcrypt',
	'pcre', 
	'pdo', 
	'pdo_mysql', 
	'simplexml'
));



$magentoModules = array(  //array that holds list of modules which we are testing against

'Codewix_Leftmenu',
'Cryozonic_Stripe',
'Cryozonic_StripeSubscriptions',
'DCKAP_Attendees',
'DCKAP_Customorder',
'DCKAP_Delegatesales',
'My_Reports',
'Nyif_Activityreport',
'Nyif_Company',
'Nyif_CourseCatalog',
'Nyif_Credits',
'Nyif_Customers',
'Nyif_Dailyreport',
'Nyif_Deferredonline',
'Nyif_Deferredreport',
'Nyif_Invoiceaging',
'Nyif_Invoicereport',
'Nyif_Onlinereport',
'Nyif_Pending',
'Nyif_Registrationreport',
'Nyif_Reports',
'Nyif_Salesbysalesreport',
'Nyif_Salesperson',
'Nyif_Signin',
'Faculty_CMS',
'Nyif_deferredonline',
'Amasty_Audit',
'Amasty_Base',
'SLandsbek_SimpleOrderExport' //csv order export
	);
$facultyPages=array(); //Array to hold faculty pages list
$main = array(); //
$installedModules = array();
$required = array(
	'general/locale/code',
	'general/locale/timezone',
	'general/locale/firstday',
	'general/locale/weekend',
	'general/store_information/name',
	'general/store_information/phone',
	'general/store_information/hours',
	'general/store_information/merchant_country',
	'general/store_information/address',
	'trans_email/ident_general/name',
	'trans_email/ident_general/email',
	'trans_email/ident_sales/name',
	'trans_email/ident_sales/email',
	'trans_email/ident_support/name',
	'trans_email/ident_support/email',
	'cms/wysiwyg/enabled',
	'catalog/frontend/list_mode',
	'design/package/name',
	'checkout/options/guest_checkout',
	'google/analytics/active',
	'google/analytics/type',
	'design/head/default_robots',
	'google/analytics/account',
	'payment/checkmo/active',
	'payment/checkmo/title',
	'payment/checkmo/sort_order',
	'payment/cryozonic_stripe/active',
	'payment/cryozonic_stripe/title',
	 'payment/cryozonic_stripe/stripe_mode',
	 'payment/cryozonic_stripe/avs',
	 'checkout/options/enable_agreements'
	);


function extension_check($extensions) {
	$fail = '';
	$pass = '';
	
	if(version_compare(phpversion(), '5.2.0', '<')) {
		$fail .= '<li>You need<strong> PHP 5.2.0</strong> (or greater)</li>';
	}
	else {
		$pass .='<li>You have<strong> PHP 5.2.0</strong> (or greater)</li>';
	}

	if(!ini_get('safe_mode')) {
		$pass .='<li>Safe Mode is <strong>off</strong></li>';
		preg_match('/[0-9]\.[0-9]+\.[0-9]+/', shell_exec('mysql -V'), $version);
		
		if(version_compare($version[0], '4.1.20', '<')) {
			$fail .= '<li>You need<strong> MySQL 4.1.20</strong> (or greater)</li>';
		}
		else {
			$pass .='<li>You have<strong> MySQL 4.1.20</strong> (or greater)</li>';
		}
	}
	else { $fail .= '<li>Safe Mode is <strong>on</strong></li>';  }

	foreach($extensions as $extension) {
		if(!extension_loaded($extension)) {
			$fail .= '<li> You are missing the <strong>'.$extension.'</strong> extension</li>';
		}
		else{	$pass .= '<li>You have the <strong>'.$extension.'</strong> extension</li>';
		}
	}
	
	if($fail) {
		echo '<p><strong>Your server does not meet the following requirements in order to install Magento.</strong>';
		echo '<br>The following requirements failed, please contact your hosting provider in order to receive assistance with meeting the system requirements for Magento:';
		echo '<ul>'.$fail.'</ul></p>';
		echo 'The following requirements were successfully met:';
		echo '<ul>'.$pass.'</ul>';
	} else {
		echo '<p><strong>Congratulations!</strong> Your server meets the requirements for Magento.</p>';
		echo '<ul>'.$pass.'</ul>';

	}
}
?>
<?php
//Check if app is alive
require 'app' . DIRECTORY_SEPARATOR . 'Mage.php';

// Setup env
error_reporting(0);

register_shutdown_function(function() use(&$step) {

    $err = error_get_last();

    if (!$err) {
        echo "OK: Database up, Magento up\n";
    } else {

        echo "KO: ";

        if (strstr($err['message'], 'PDOException') !== false) {
            echo "Database down, Magento down\n";
        } else {
            echo "Database up, Magento down\n";

        }
    }
});

Mage::app('admin', 'store', array(
    'global_ban_use_cache' => true
));

?>
<h1>Go Live Checklist</h1>
<?php 

//Connecting to a DB used in Magento
$xml = simplexml_load_file('app/etc/local.xml', NULL, LIBXML_NOCDATA);

$db['host'] = $xml->global->resources->default_setup->connection->host;
$db['name'] = $xml->global->resources->default_setup->connection->dbname;
$db['user'] = $xml->global->resources->default_setup->connection->username;
$db['pass'] = $xml->global->resources->default_setup->connection->password;
$db['pref'] = $xml->global->resources->db->table_prefix;




$servername = $db['host'];
$username = $db['user'];
$password = $db['pass'];
$dbname = $db['name'];


$link = mysql_connect($servername, $username, $password);
mysql_select_db($dbname, $link);

// Calling out Mage app to access magento related stuff.
require_once 'app/Mage.php';
Mage::app();

 

$attributeOptions =  Mage::getModel('cms/page')->getCollection()->toOptionArray();
foreach ($attributeOptions as $teacher => $value) {
	// var_dump($value);
	if(strpos($value['value'], "faculty/")!==FALSE){   
		$facultyPages[]=$value['value'];
	}
}


/*Instructor Options*/
?>
<h1>instructor attribute's Options(Faculty)</h1>
<table>
<?php
$page = Mage::getModel('cms/page');
$page->setStoreId(Mage::app()->getStore()->getId());
$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'instructor');
if ($attribute->usesSource()) {
    $options = $attribute->getSource()->getAllOptions(false);
    foreach ($options as $faculty => $member) {
    	// var_dump($member);
    	echo "<tr><td>".$member['label']."</td>";
    	$imagepath = 'skin/frontend/nyif/default/images/instructors/';  //Not dynamic change
    	$pic = explode(", ", $member['label']);
    	$filename = $pic[1].'-'.$pic[0].'.jpg';
    	$linkname = strtolower('faculty/'.$pic[0]."-".$pic[1]);
?>
<td>
<?php
//Check if we have image associated with instructor
if (file_exists($imagepath.$filename)) {
    echo "<span class='green'> Has Profile Pic</span><img src='".$imagepath.$filename."' width='10' hieght='10'/>";
} else {
    echo "<span class='red'> !NO PICTURE</span>";
}
?>
</td>
<?php
// echo $linkname;
//Check if we have page associated with instructor
?>
<td>
<?php
if (in_array($linkname, $facultyPages)) {
    echo "<span class='green'><a href='$linkname'> Has Page</a></span>";
} else {
    echo "<span class='red'> !NO Page</span><";
}
?>
</td>
<?php
//Check if JSON is valid

$page->load($linkname,'identifier');

$helper = Mage::helper('cms');
$processor = $helper->getPageTemplateProcessor();
$html = $processor->filter($page->getContent());
?>
<td>
<?php
if (is_object(json_decode($html))) 
    { 
       echo "<span class='green' title='$html'> JSON valid</span>";
    }else{
    	echo "<span class='red invalidJSON' title='$html'> invalid JSON</span>";
    }
?>
		</td>
   </tr>

   <?php
    	
    }
}
?>
</table>
<?php
//Get faculty pages



//Get list of all installed Modules
?>
<h1> Required MODULES </h1>
<?php
$modules = Mage::getConfig()->getNode('modules')->children();
foreach ($modules as $module => $value){
	# code...
	// echo $module." ".(in_array($module,$magentoModules) ? '<span style="color:red;" > *Required</span>' : '')."<br/>";
	$installedModules[] = $module; 
	
}
foreach ($magentoModules as $magentoModule) {
	# code...
	if(in_array($magentoModule, $installedModules)){
		echo $magentoModule." <span style='color:green;' >installed</span><br/>";
	}else{
		echo $magentoModule." <span style='color:red;' >missing</span><br/>";
	}
}





/*Getting CMS STATIC BLOCK DATA*/
/*Getting base Magento config DATA*/
$result = mysql_query("SELECT * FROM  cms_block", $link);

$num_rows = mysql_num_rows($result);


// $checklist = mysql_fetch_assoc($result);

?>
<h1> CMS STATIC BLOCKS </h1>
<?php
echo "$num_rows Rows <br/>";
while ($row = mysql_fetch_assoc($result)) {
			
	echo ($row['is_active'] > 0 ? '<span style="color:green;" >Active</span>' : '<span style="color:red;" >Not Active</span>')."==>".$row['block_id']." ==> ".$row['identifier']." ==> ".$row['title']."<br/>";

}




/*Getting base Magento config DATA*/
?>
<h1> Magento Configuration </h1>
<?php
$result = mysql_query("SELECT * FROM core_config_data", $link);

$num_rows = mysql_num_rows($result);
echo "$num_rows Rows\n";

// $checklist = mysql_fetch_assoc($result);
/*Set up grouper element to group rows by first in first/second/ */
$grouper ='newest';
while ($row = mysql_fetch_assoc($result)) {

	$current = explode("/",$row['path']);
	$current = $current[0];
	if($grouper!=$current){
		?>
		<h2><?php // echo $current; ?> </h2>
		<?php
		$grouper = $current;
	}else{

	}

	if(in_array($row['path'],$required)){
		$main[]=$row;
	}

	//echo $row['path']." ==> ".$row['value'].(in_array($row['path'],$required)?'<span style="color:red;" > *Required</span>' : '')."<br/>";

}





// foreach ($checklist as $key => $value) {
// 	# code...
// 	echo $key." ==> ".$value."<br/>";
// }


// // Create connection
// $conn = new mysqli($servername, $username, $password, $dbname);
// // Check connection
// if ($conn->connect_error) {
//     die("Connection failed: " . $conn->connect_error);
// } 

// $sql = "SELECT * FROM core_config_data";
// $result = $conn->query($sql);

// if ($result->num_rows > 0) {
//     // output data of each row
//     while($row = $result->fetch_assoc()) {
//         echo $row. "<br>";
//     }
// } else {
//     echo "0 results";
// }
 
// var_dump($main);
?> <h1>Magento Configuration Checklist</h1> 
<table>
<?php

foreach ($main as $key => $row) {
	?> <tr><?php
		echo '<td>'.$row['path']."</td><td> ".$row['value']."</td>";
	?>
	</tr>
	<?php
}

?>
</table>


