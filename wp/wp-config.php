<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'greenstore_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'p2s(6WyW5er&ZM{');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


define('WP_HOME','http://blog.greenbuyback.com');
define('WP_SITEURL','http://blog.greenbuyback.com');




/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3+5Ftd6cC/JR_&KO1=Iv@9L+0kXp4K0;nj-^(;JroQoCddEYUBcv_|_jSHMIwof]');
define('SECURE_AUTH_KEY',  '+TjR5_]l@?z>-+Je;[b<Odr!k-q9?y+Z) 4Y9:XHqW+C]d-|x^#jck4>_R2Shg]S');
define('LOGGED_IN_KEY',    '&y1|)5gIlx--mSdLeu58/QW-y.%n.Rj,t`bG5_N`%B[f=V@rTRba q;A9Z|T^N&^');
define('NONCE_KEY',        'Cc$wN*/ztyht Mo`O,TW|Z@@70F2ms3WCELbF)+8Xe^.$2ddR.f-P*:-m4~#`;5r');
define('AUTH_SALT',        '{?vN!CL@hzQfilD9|)@7;WLXq-i=*&hS WHdLE;jiPC[B59-/u@j;*q+]nEswT0k');
define('SECURE_AUTH_SALT', 'HY0iO~^q9SQI,dW+95leO(:xJ(tz_myW9yT(VpCmp|RlI6Z: G[d!sMvJ#hjS8L}');
define('LOGGED_IN_SALT',   'B?#>+Myo+Ey-/)&B0F ZWo-sif$b~W}AY44 Arw|GXj7qjs|d/_tc9_`1^|,~h+a');
define('NONCE_SALT',       'zze*(Q~Boz?w~+HzqaD<`QpF^(s(fCke+VS[@iFn`=j%_[y nlUYUD]+UZ*Y-%C`');






/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
