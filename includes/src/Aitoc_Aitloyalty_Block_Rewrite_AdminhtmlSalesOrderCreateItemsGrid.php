<?php
/**
 * Loyalty Program
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitloyalty
 * @version      2.3.17
 * @license:     Kp3auyVhQ0x7OAufhTieSqVe2sWXaoRky2ijjO5VCk
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */

class Aitoc_Aitloyalty_Block_Rewrite_AdminhtmlSalesOrderCreateItemsGrid extends Mage_Adminhtml_Block_Sales_Order_Create_Items_Grid
{
	protected function _afterToHtml($html)
	{
		$html = str_replace('<th class="no-link">' . Mage::helper('sales')->__('Discount') . '</th>', '<th class="no-link">' . Mage::helper('sales')->__('Discount/Surcharge') . '</th>', $html);
		return $html;
	}
}