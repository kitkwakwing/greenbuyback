<?php
/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php

class Infomodus_Upslabelinv_Adminhtml_UpslabelinvController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('upslabelinv/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function showlabelAction()
    {
        $configOptions = new Infomodus_Upslabelinv_Model_Config_Options;
        $configMethod = new Infomodus_Upslabelinv_Model_Config_Upsmethod;
        $order_id = $this->getRequest()->getParam('order_id');
        $type = $this->getRequest()->getParam('type');
        $params = $this->getRequest()->getParams();
        $this->imOrder = Mage::getModel('sales/order')->load($order_id);
        $this->loadLayout();
        $block = $this->getLayout()->getBlock('showlabel');
        $AccessLicenseNumber = Mage::getStoreConfig('upslabelinv/profile/accesslicensenumber');
        $UserId = Mage::getStoreConfig('upslabelinv/profile/userid');
        $Password = Mage::getStoreConfig('upslabelinv/profile/password');
        $shipperNumber = Mage::getStoreConfig('upslabelinv/profile/shippernumber');

        $path = Mage::getBaseDir('media') . DS . 'upslabelinv' . DS . 'label' . DS;

        $lbl = new Infomodus_Upslabelinv_Model_Ups();

        $lbl->setCredentials($AccessLicenseNumber, $UserId, $Password, $shipperNumber);
        $collections = Mage::getModel('upslabelinv/upslabelinv');
        $colls = $collections->getCollection()->addFieldToFilter('order_id', $order_id)->addFieldToFilter('type', $type);
        $coll = 0;
        foreach ($colls AS $k => $v) {
            $coll = $k;
            break;
        }
        $collection = Mage::getModel('upslabelinv/upslabelinv')->load($coll);
        if ($collection->getOrderId() != $order_id) {
            $lbl->shipmentDescription = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shipmentdescription']);
            $lbl->shipperName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/name'));
            $lbl->shipperAttentionName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/attentionname'));
            $lbl->shipperPhoneNumber = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/phonenumber'));
            $lbl->shipperAddressLine1 = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/addressline1'));
            $lbl->shipperCity = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/city'));
            $lbl->shipperStateProvinceCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/stateprovincecode'));
            $lbl->shipperPostalCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/postalcode'));
            $lbl->shipperCountryCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/countrycode'));

            $lbl->shiptoCompanyName = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptocompanyname']);
            $lbl->shiptoAttentionName = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptoattentionname']);
            $lbl->shiptoPhoneNumber = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptophonenumber']);
            $lbl->shiptoAddressLine1 = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptoaddressline1']);
            $lbl->shiptoCity = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptocity']);
            $lbl->shiptoStateProvinceCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($configOptions->getProvinceCode($params['shiptostateprovincecode']));
            $lbl->shiptoPostalCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptopostalcode']);
            $lbl->shiptoCountryCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['shiptocountrycode']);
            $lbl->residentialAddress = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['residentialaddress']);
            $shipfrom = 'shipfrom';
            if ($type == 'from') {
                $shipfrom = 'shipto';
            }
            $lbl->shipfromCompanyName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/companyname'));
            $lbl->shipfromAttentionName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/attentionname'));
            $lbl->shipfromPhoneNumber = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/phonenumber'));
            $lbl->shipfromAddressLine1 = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/addressline1'));
            $lbl->shipfromCity = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/city'));
            $lbl->shipfromStateProvinceCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/stateprovincecode'));
            $lbl->shipfromPostalCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/postalcode'));
            $lbl->shipfromCountryCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/' . $shipfrom . '/countrycode'));

            $lbl->serviceCode = $params['serviceCode'];
            $lbl->serviceDescription = $configMethod->getUpsMethodName($params['serviceCode']);

            $lbl->packageWeight = $params['weight'] + (is_numeric(str_replace(',', '.', $params['packweight'])) ? $params['packweight'] : 0);
            $lbl->weightUnits = $params['weightunits'];
            $lbl->weightUnitsDescription = $params['weightunitsdescription'];
            $lbl->largePackageIndicator = $params['largepackageindicator'];

            $lbl->includeDimensions = $params['includedimensions'];
            $lbl->unitOfMeasurement = $params['unitofmeasurement'];
            $lbl->unitOfMeasurementDescription = $params['unitofmeasurementdescription'];
            $lbl->length = $params['length'];
            $lbl->width = $params['width'];
            $lbl->height = $params['height'];


            $lbl->packagingTypeCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['packagingtypecode']);
            $lbl->packagingDescription = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['packagingdescription']);
            $lbl->packagingReferenceNumberCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['packagingreferencenumbercode']);
            $lbl->packagingReferenceNumberValue = Infomodus_Upslabelinv_Helper_Help::escapeXML($params['packagingreferencenumbervalue']);

            $lbl->codMonetaryValue = $this->imOrder->getGrandTotal();

            if ($type != 'from') {
                $upsl = $lbl->getShip();
            }
            else {
                $upsl = $lbl->getShipFrom();
            }
            if (!array_key_exists('error', $upsl) || !$upsl['error']) {
                $upslabel = Mage::getModel('upslabelinv/upslabelinv');
                $upslabel->setTitle('Order ' . $order_id . ' TN' . $upsl['trackingnumber']);
                $upslabel->setOrderId($order_id);
                $upslabel->setType($type);
                $upslabel->setTrackingnumber($upsl['trackingnumber']);
                $upslabel->setShipmentidentificationnumber($upsl['shipidnumber']);
                $upslabel->setShipmentdigest($upsl['digest']);
                $upslabel->setLabelname($upsl['labelname']);
                $upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
                $upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
                $upslabel->save();
                /*$backLink = $this->getUrl('adminhtml/sales_order_shipment/new/order_id/' . $order_id);*/
                $backLink = $params['referer'];
                /*if ($params['addtrack'] == 1) {
                    $trTitle = 'United Parcel Service';
                    $shipment = Mage::getModel('sales/order_shipment')->load($shipment_id);
                    $track = Mage::getModel('sales/order_shipment_track')
                        ->setNumber(trim($upsl['trackingnumber']))
                        ->setCarrierCode('ups')
                        ->setTitle($trTitle);
                    $shipment->addTrack($track)->save();
                }*/
                $shipMethodArray = explode('_', $this->imOrder->getShippingMethod());
                $shipWay = 0;
                if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
                    $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
                    $shipWay = $upstablerates['way'];
                }
                /*if ($type == 'from' && (int)$shipWay != 3) {
                    $customer = Mage::getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->load($this->imOrder->getCustomerId());
                    $customerEmail = $customer->getEmail();
                    $customerFirstname = $customer->getFirstname();
                    $customerLastname = $customer->getLastname();

                    $mail = new Zend_Mail();
                    $mail->setType(Zend_Mime::MULTIPART_RELATED);
                    $mail->setBodyHtml('UPS Shippinl Label is attached to this message.<br />If you don\'t see this image, please ckick on the link: <a href="' . $this->getUrl('upslabelinv/label/view/order_id/' . $order_id) . '">' . $this->getUrl('upslabelinv/label/view/order_id/' . $order_id) . '</a>');
                    $fileGif = $mail->createAttachment(file_get_contents(Mage::getBaseUrl('media') . 'upslabelinv/label/' . $upsl['labelname']),
                        'image/gif',
                        Zend_Mime::DISPOSITION_INLINE,
                        Zend_Mime::ENCODING_BASE64);
                    $fileGif->filename = 'label.gif';
                    $mail->setFrom(Mage::getStoreConfig('upslabelinv/way/emailusersend'), Mage::app()->getStore()->getName());

                    $mail->addTo($customerEmail, $customerFirstname . ' ' . $customerLastname);

                    $mail->setSubject('Ups label for order number ' . $this->imOrder->getIncrementId());
                    $mail->send();
                }*/
                $block->assign('order_id', $order_id);
                $block->assign('upsl', $upsl);
                $block->assign('backLink', $backLink);
                $block->assign('type', $type);
                $block->assign('error', array());
            }
            else {
                $block->assign('error', $upsl);
            }
        }
        else {
            /*$backLink = $this->getUrl('adminhtml/sales_order_shipment/new/order_id/' . $order_id);*/
            $backLink = $_SERVER['HTTP_REFERER'];
            $block->assign('order_id', $collection->getOrderId());
            $block->assign('upsl', $collection);
            $block->assign('backLink', $backLink);
            $block->assign('type', $type);
            $block->assign('error', array());
        }
        $this->renderLayout();
    }

    public function intermediateAction()
    {
        $configOptions = new Infomodus_Upslabelinv_Model_Config_Options;
        $configMethod = new Infomodus_Upslabelinv_Model_Config_Upsmethod;
        $this->loadLayout();
        $block = $this->getLayout()->getBlock('intermediate');
        $order_id = $this->getRequest()->getParam('order_id');
        $type = $this->getRequest()->getParam('type');
        $this->imOrder = Mage::getModel('sales/order')->load($order_id);
        $block->assign('order', $this->imOrder);
        $block->assign('shipTo', $this->imOrder->getShippingAddress());
        $this->imShipment = Mage::getModel('sales/order_shipment')->load($order_id)->getShipping();
        $block->assign('shipment', $this->imShipment);
        $block->assign('type', $type);
        /*$shipmentAllItems = $this->imShipment->getAllItems();
        $totalPrice = 0;
        $totalWight = 0;
        $imProduct = array();
        foreach ($shipmentAllItems AS $item) {
            $imProduct = Mage::getModel('catalog/product')->load($item->getProductId());
            $totalPrice += $imProduct->getPrice() * $item->getQty();
            $totalWight += $imProduct->getWeight() * $item->getQty();
        }*/
        $block->assign('shipmentTotalWeight', $this->imOrder->getWeight());
        $ship_method = $this->imOrder->getShippingMethod();
        $shipByUps = preg_replace("/^ups_.{1,4}$/", 'ups', $ship_method);
        $shipByUpsCode = $configMethod->getUpsMethodNumber(preg_replace("/^ups_(.{2,4})$/", '$1', $ship_method));
        //echo $shipByUpsCode;
        $shipMethodArray = explode('_', $this->imOrder->getShippingMethod());
        $shipWay = 0;
        if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
            $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
            $shipByUps = 'upstablerates';
            $Code_['to'] = 1;
            $Code_['from'] = 1;
            $Code_['to2'] = 1;
            $Code = explode('-', $upstablerates['method_code']);
            switch (count($Code)) {
                case 1:
                    $Code_['to'] = $Code[0];
                    $Code_['from'] = $Code[0];
                    $Code_['to2'] = $Code[0];
                    break;
                case 3:
                    $Code_['to'] = $Code[0];
                    $Code_['from'] = $Code[1];
                    $Code_['to2'] = $Code[2];
                    break;
                case 2:
                    $shipMethodArray = explode('_', $this->imOrder->getShippingMethod());
                    $shipWay = 0;
                    if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
                        $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
                        $shipWay = $upstablerates['way'];
                    }
                    if ((int)$shipWay == 3) {
                        $Code_['to'] = $Code[0];
                        $Code_['from'] = $Code[1];
                        $Code_['to2'] = $Code[1];
                    }
                    else {
                        $Code_['to'] = $Code[0];
                        $Code_['from'] = $Code[1];
                        $Code_['to2'] = $Code[0];
                    }
                    break;
            }
            $shipByUpsCode = $configMethod->getUpsMethodNumber($Code_[$type]);
        }
        $block->assign('shipByUps', $shipByUps);
        $block->assign('shipByUpsCode', $shipByUpsCode);
        $shipByUpsMethodName = $configMethod->getUpsMethodName($shipByUpsCode);
        $block->assign('shipByUpsMethodName', $shipByUpsMethodName);
        $block->assign('shipByUpsMethods', $configMethod->getUpsMethods());
        $block->assign('unitofmeasurement', $configOptions->getUnitOfMeasurement());
        $block->assign('paymentmethod', $this->imOrder->getPayment()->getMethodInstance()->getCode());
        $block->assign('referer', $_SERVER['HTTP_REFERER']);
        $this->renderLayout();
    }

    public function deletelabelAction()
    {
        $order_id = $this->getRequest()->getParam('order_id');
        $this->loadLayout();
        $this->_addLeft($this->getLayout()->createBlock('upslabelinv/adminhtml_upslabelinv_label_del'));
        $this->renderLayout();
    }

    public function printAction()
    {
        $imname = $this->getRequest()->getParam('imname');
        $path = Mage::getBaseDir('media') . DS . 'upslabelinv' . DS . 'label' . DS;
        $path_url = Mage::getBaseUrl('media') . DS . 'upslabelinv' . DS . 'label' . DS;
        $imageVerticalNameUrl = $path_url . $imname;
        if (Mage::getStoreConfig('upslabelinv/labeloptions/verticalprint') == 1) {
            $imageVerticalName = 'vertical/vertical' . str_replace('.gif', '.jpg', $imname);
            $imageVerticalNamePath = $path . $imageVerticalName;
            $imageVerticalNameUrl = $path_url . $imageVerticalName;
            if (!file_exists($imageVerticalNamePath)) {
                if (!is_dir($path . 'vertical')) {
                    mkdir($path . 'vertical' . DS, 0777);
                }
                $f_cont = file_get_contents($path_url . $imname);
                $img = imagecreatefromstring($f_cont);
                $FullImage_width = imagesx($img);
                $FullImage_height = imagesy($img);
                $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                $col = imagecolorallocate($img, 125, 174, 240);
                $full_id = imagerotate($img, -90, $col);
                imagejpeg($full_id, $imageVerticalNamePath);
            }
        }

        echo '<html>
<head>
<title>Print Shipping Label</title>
</head>
<body>
<style>
img {
' . (Mage::getStoreConfig('upslabelinv/labeloptions/verticalprint') == 1 ? 'height:100%;' : 'width:100%;') . '
}
</style>
<img src="' . $imageVerticalNameUrl . '" />
<script>
window.onload = function(){window.print();}
</script>
</body>
</html>';
        exit;
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('upslabelinv/upslabelinv')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('upslabelinv_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('upslabelinv/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('upslabelinv/adminhtml_upslabelinv_edit'))
                ->_addLeft($this->getLayout()->createBlock('upslabelinv/adminhtml_upslabelinv_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('upslabelinv')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('upslabelinv/upslabelinv');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));

            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }

                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('upslabelinv')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('upslabelinv')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('upslabelinv/upslabelinv');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $upslabelIds = $this->getRequest()->getParam('upslabelinv');
        if (!is_array($upslabelIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($upslabelIds as $upslabelId) {
                    $upslabel = Mage::getModel('upslabelinv/upslabelinv')->load($upslabelId);
                    $upslabel->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($upslabelIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $upslabelIds = $this->getRequest()->getParam('upslabelinv');
        if (!is_array($upslabelIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($upslabelIds as $upslabelId) {
                    $upslabel = Mage::getSingleton('upslabelinv/upslabelinv')
                        ->load($upslabelId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($upslabelIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'upslabelinv.csv';
        $content = $this->getLayout()->createBlock('upslabelinv/adminhtml_upslabelinv_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'upslabelinv.xml';
        $content = $this->getLayout()->createBlock('upslabelinv/adminhtml_upslabelinv_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

}