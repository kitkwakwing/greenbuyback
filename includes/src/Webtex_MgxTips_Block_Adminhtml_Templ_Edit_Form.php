<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Adminhtml Newsletter Template Edit Form Block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Webtex_MgxTips_Block_Adminhtml_Templ_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Define Form settings
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function _getOpt()
    {
        $data = $this->getRequest()->getParams();
        
        $collection = Mage::getModel('mgxtips/templates')->getCollection()
            ->addFieldToFilter('mgxtips_templates_id', $data['template_id']);
            
        return  $collection->getFirstItem()->getOptionId();  
    }
    
    
    protected function _getVal($o)
    {
        $collection = Mage::getModel('mgxtips/templates')->getCollection()
            ->addFieldToFilter('option_id', $this->_getOpt())
            ->addFieldToFilter('store_id', $o);

        return $collection->getFirstItem()->getValue();                                             
    }
    
    protected function _getId($o)
    {
        $collection = Mage::getModel('mgxtips/templates')->getCollection()
            ->addFieldToFilter('option_id', $this->_getOpt())
            ->addFieldToFilter('store_id', $o);

        return $collection->getFirstItem()->getId();                                             
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Newsletter_Template_Edit_Form
     */
    protected function _prepareForm()
    {
        $form   = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'method'    => 'post'
        ));

        $fieldset   = $form->addFieldset('base_fieldset', array(
            'legend'    => Mage::helper('newsletter')->__('Add'),
            'class'     => 'fieldset-wide'
        ));
        
        
        $fieldset->addField('code', 'text', array(
            'name'      => 'option_id',
            'label'     => Mage::helper('newsletter')->__('Snippet Name'),
            'title'     => Mage::helper('newsletter')->__('Snippet Name'),
            'required'  => true,
            'value'     => $this->_getOpt(),
        ));
        
        
        foreach ($this->getStores() as $_store)
        {
            $fieldset->addField('text'.$_store->getId(), 'editor', array(
                'name'      => 'value['.$this->_getId($_store->getId()).']',
                'label'     => Mage::helper('newsletter')->__('Template Content for <b>'.$_store->getName().'</b>'),
                'title'     => Mage::helper('newsletter')->__($_store->getName()),
                'required'  => false,
                'state'     => 'html',
                'style'     => 'height:26em;',
                'value'     => $this->_getVal($_store->getId()),
                'class'     => 'textarea-up'
            ));      
        }
        

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    public function getStores()
    {
        $stores = $this->getData('stores');
        if (is_null($stores)) {
            $stores = Mage::getModel('core/store')
                ->getResourceCollection()
                ->setLoadDefault(true)
                ->load();
            $this->setData('stores', $stores);
        }
        return $stores;
    }
}
