<?php
/**
 * Webtex
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.webtexsoftware.com/LICENSE.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@webtexsoftware.com and we will send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to http://www.webtexsoftware.com for more information, 
 * or contact us through this email: info@webtexsoftware.com.
 *
 * @category   MagExt
 * @package    MagExt_Tips
 * @copyright  Copyright (c) 2011 Webtex Solutions, LLC (http://www.webtexsoftware.com/)
 * @license    http://www.webtexsoftware.com/LICENSE.txt End-User License Agreement
 */
 
class MagExt_MgxTips_Block_Adminhtml_Templ_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('mgxtips_templates');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mgxtips/templates')->getCollection();
        $collection->getSelect()->group('option_id');
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        
        foreach($this->getCollection() as $col)
        {
            $col->setLastname($col->getLastname() . ' ' . $col->getFirstname());
        }
        
        return $this;
    }

    protected function _prepareColumns()
    {       
        $this->addColumn('option_id', array(
            'header'    => Mage::helper('mgxtips')->__('Snippet Name'),
            'align'     => 'left',
            'sortable'  => true,
            'index'     => 'option_id',
        ));
        
        $this->addColumn('action',
            array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('Delete'),
                            'url'     => array('base'=>'*/*/delete'),
                            'field'   => 'template_id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'is_system' => true,
        ));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('template_id'=>$row->getMgxtipsTemplatesId()));
    }

}
