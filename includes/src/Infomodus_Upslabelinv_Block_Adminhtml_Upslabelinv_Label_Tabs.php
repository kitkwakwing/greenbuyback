<?php

/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php

class Infomodus_Upslabelinv_Block_Adminhtml_Upslabelinv_Label_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    private $_order_id = false;
    private $_direction = false;
    private $_without = false;

    protected function _beforeToHtml()
    {
        $AccessLicenseNumber = Mage::getStoreConfig('upslabelinv/profile/accesslicensenumber');
        $UserId = Mage::getStoreConfig('upslabelinv/profile/userid');
        $Password = Mage::getStoreConfig('upslabelinv/profile/password');
        $shipperNumber = Mage::getStoreConfig('upslabelinv/profile/shippernumber');

        $configOptions = new Infomodus_Upslabelinv_Model_Config_Options;
        $configMethod = new Infomodus_Upslabelinv_Model_Config_Upsmethod;

        $order_id = $this->_order_id !== false ? $this->_order_id : $this->getRequest()->getParam('order_id');
        $direction = $this->_direction !== false ? $this->_direction : $this->getRequest()->getParam('direction');

        $path = Mage::getBaseDir('media') . DS . 'upslabelinv' . DS . 'label' . DS;

        $lbl = new Infomodus_Upslabelinv_Model_Ups();

        $lbl->setCredentials($AccessLicenseNumber, $UserId, $Password, $shipperNumber);


        $collection1 = Mage::getModel('upslabelinv/upslabelinv')->getCollection()->addFieldToFilter('type', $direction)->addFieldToFilter('order_id', $order_id);
        if ($collection1->getSize() > 0) {
            $collection = $collection1->getData();
            $collection = $collection[0];
        }
        if ($collection1->getSize() == 0) {
            $order = Mage::getModel('sales/order')->load($order_id);
            $shiping_adress = $order->getShippingAddress();
            $ship_method = $order->getShippingMethod();
            $shipByUps = preg_replace("/^ups_.{2,4}$/", 'ups', $ship_method);
            $onlyups = Mage::getStoreConfig('upslabelinv/profile/onlyups');
            if ($shipByUps == 'ups' || $onlyups == 0) {
                $shipMethodArray = explode('_', $ship_method);
                $shipByUpsCode = preg_replace("/^ups_(.{2,4})$/", '$1', $ship_method);
                $db = Mage::getSingleton('core/resource')->getConnection('core_write');
                $shipByUpsName = $db->query('SELECT method_title FROM ' . Mage::app()->getConfig()->getNode('global/resources/db')->table_prefix . 'sales_flat_quote_shipping_rate where code=\'' . $ship_method . '\'')->fetch();
                $lbl->shipmentDescription = $this->getRequest()->getParam('description') ? Infomodus_Upslabelinv_Helper_Help::escapeXML($this->getRequest()->getParam('description')) : Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/profile/description'));
                $lbl->shipperName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/name'));
                $lbl->shipperAttentionName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/attentionname'));
                $lbl->shipperPhoneNumber = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/phonenumber'));
                $lbl->shipperAddressLine1 = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/addressline1'));
                $lbl->shipperCity = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/city'));
                $lbl->shipperStateProvinceCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/stateprovincecode'));
                $lbl->shipperPostalCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/postalcode'));
                $lbl->shipperCountryCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/shipper/countrycode'));

                $lbl->shiptoCompanyName = strlen($shiping_adress['company']) == 0 ? (strlen(Mage::getStoreConfig('upslabelinv/profile/companyname')) > 0 ? Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/profile/companyname')) : Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['firstname'] . ' ' . $shiping_adress['lastname'])) : Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['company']);
                $lbl->shiptoAttentionName = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['firstname'] . ' ' . $shiping_adress['lastname']);
                $lbl->shiptoPhoneNumber = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['telephone']);
                $lbl->shiptoAddressLine1 = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['street']);
                $lbl->shiptoCity = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['city']);
                $lbl->shiptoStateProvinceCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($configOptions->getProvinceCode($shiping_adress['region']));
                $lbl->shiptoPostalCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['postcode']);
                $lbl->shiptoCountryCode = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['country_id']);
                $lbl->shiptoCustomerEmail = Infomodus_Upslabelinv_Helper_Help::escapeXML($shiping_adress['email']);
                $shipfrom = 'shipfrom';
                if ($direction == 'from') {
                    $shipfrom = 'shipto';
                }
                $lbl->shipfromCompanyName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/companyname'));
                $lbl->shipfromAttentionName = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/attentionname'));
                $lbl->shipfromPhoneNumber = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/phonenumber'));
                $lbl->shipfromAddressLine1 = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/addressline1'));
                $lbl->shipfromCity = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/city'));
                $lbl->shipfromStateProvinceCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/stateprovincecode'));
                $lbl->shipfromPostalCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/postalcode'));
                $lbl->shipfromCountryCode = Infomodus_Upslabelinv_Helper_Help::escapeXML(Mage::getStoreConfig('upslabelinv/'.$shipfrom.'/countrycode'));
                $lbl->weightUnits = Mage::getStoreConfig('upslabelinv/profile/weightunits');
                $lbl->weightUnitsDescription = '';

                if ($shipMethodArray[0] == 'upstablerates') {
                    $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates_collection');
                    $modeltableratesData = $upstablerates->getData();
                    foreach ($modeltableratesData AS $v) {
                        if ($shipMethodArray[2] == $v['pk']) {
                            $toCode = 1;
                            $fromCode = 1;
                            $to2Code = 1;
                            $Code = explode('-', $v['method_code']);
                            switch (count($Code)) {
                                case 1:
                                    $toCode = $Code[0];
                                    $fromCode = $Code[0];
                                    $to2Code = $Code[0];
                                    break;
                                case 3:
                                    $toCode = $Code[0];
                                    $fromCode = $Code[1];
                                    $to2Code = $Code[2];
                                    break;
                                case 2:
                                    $shipMethodArray = explode('_', $order->getShippingMethod());
                                    $shipWay = 0;
                                    if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
                                        $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
                                        $shipWay = $upstablerates['way'];
                                    }
                                    if ((int)$shipWay == 3) {
                                        $toCode = $Code[0];
                                        $fromCode = $Code[1];
                                        $to2Code = $Code[1];
                                    }
                                    else {
                                        $toCode = $Code[0];
                                        $fromCode = $Code[1];
                                        $to2Code = $Code[0];
                                    }
                                    break;
                            }
                            switch ($direction) {
                                case 'to':
                                    $lbl->serviceCode = $toCode;
                                    break;
                                case 'from':
                                    $lbl->serviceCode = $fromCode;
                                    break;
                                case 'to2':
                                    $lbl->serviceCode = $to2Code;
                                    break;
                            }
                        }
                    }
                    //var_dump($modeltablerates->getData()); exit;
                } else {
                    $lbl->serviceCode = $configMethod->getUpsMethodNumber($shipByUpsCode);
                }
                $lbl->serviceCode = $configMethod->getUpsMethodNumber((int)$lbl->serviceCode);

                $lbl->serviceDescription = $shipByUpsName['method_title'];

                $lbl->packageWeight = $order['weight'];

                $lbl->packagingTypeCode = Mage::getStoreConfig('upslabelinv/profile/packagingtypecode');
                $lbl->packagingDescription = Mage::getStoreConfig('upslabelinv/profile/packagingdescription');
                $lbl->packagingReferenceNumberCode = Mage::getStoreConfig('upslabelinv/profile/packagingreferencenumbercode');
                $lbl->packagingReferenceNumberValue = Mage::getStoreConfig('upslabelinv/profile/packagingreferencenumbervalue');
                $this->codMonetaryValue = $order->getGrandTotal();

                if ($direction == 'to' || $direction == 'to2') {
                    $upsl = $lbl->getShipTo($order_id);
                }
                else if ($direction == 'from') {
                    $upsl = $lbl->getShipFrom($order_id);
                }
                if (!array_key_exists('error', $upsl)) {
                    $upslabel = Mage::getModel('upslabelinv/upslabelinv');
                    $upslabel->setTitle('Order ' . $order_id . ' TN' . $upsl['trackingnumber']);
                    $upslabel->setOrderId($order_id);
                    $upslabel->setTrackingnumber($upsl['trackingnumber']);
                    $upslabel->setShipmentidentificationnumber($upsl['shipidnumber']);
                    $upslabel->setShipmentdigest($upsl['digest']);
                    $upslabel->setLabelname($upsl['labelname']);
                    $upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
                    $upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
                    $upslabel->setType($direction);
                    $upslabel->save();

                    $shipMethodArray = explode('_', $order->getShippingMethod());
                    $shipWay = 0;
                    if ($shipMethodArray[0] == 'upstablerates' && count($shipMethodArray) > 2) {
                        $upstablerates = Mage::getResourceModel('upstablerates_shipping/carrier_upstablerates')->loadPk($shipMethodArray[2]);
                        $shipWay = $upstablerates['way'];
                    }
                    /*if ($direction == 'from' && (int)$shipWay != 3 && (Mage::getStoreConfig('upslabelinv/way/emailusend') == 1 || ($this->_direction === false && Mage::getStoreConfig('upslabelinv/way/emailusend') == 0))) {
                        $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                            ->load($order->getCustomerId());
                        $customerEmail = $customer->getEmail();
                        $customerFirstname = $customer->getFirstname();
                        $customerLastname = $customer->getLastname();

                        $mail = new Zend_Mail();
                        $mail->setType(Zend_Mime::MULTIPART_RELATED);
                        $mail->setBodyHtml('UPS Shippinl Label is attached to this message.<br />If you don\'t see this image, please ckick on the link: <a href="' . $this->getUrl('upslabelinv/label/view/order_id/' . $order_id) . '">' . $this->getUrl('upslabelinv/label/view/order_id/' . $order_id) . '</a>');
                        $fileGif = $mail->createAttachment(file_get_contents(Mage::getBaseUrl('media') . 'upslabelinv/label/' . $upsl['labelname']),
                            'image/gif',
                            Zend_Mime::DISPOSITION_INLINE,
                            Zend_Mime::ENCODING_BASE64);
                        $fileGif->filename = 'label.gif';
                        $mail->setFrom(Mage::getStoreConfig('upslabelinv/way/emailusersend'), Mage::app()->getStore()->getName());

                        $mail->addTo($customerEmail, $customerFirstname . ' ' . $customerLastname);

                        $mail->setSubject('Ups label for order number ' . $order->getIncrementId());
                        $mail->send();
                    }*/
                } else {
                    if ($this->_without === false) {
                        Mage::log($upsl['error']);
                    }
                    else {
                        $mail = new Zend_Mail();
                        $mail->setBodyText($upsl['error']);
                        $mail->setFrom(Mage::getStoreConfig('upslabelinv/labeloptions/emailerror'), Mage::app()->getStore()->getName());
                        $mail->addTo(Mage::getStoreConfig('upslabelinv/labeloptions/emailerror'), 'Error');
                        $mail->setSubject('Ups label error for order number ' . $order->getIncrementId() . ' and type ' . $direction);
                        $mail->send();
                    }

                }
            }
        }
    }

    public function createLabel($order, $direction)
    {

        $this->_direction = $direction;
        $this->_order_id = $order;
        $this->_without = true;
        $this->_beforeToHtml();
    }

}